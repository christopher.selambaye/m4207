#include <LWiFi.h>
#include <LWiFiUdp.h>
#include <LFlash.h>
#include <LSD.h>
#include <LStorage.h>

#define Drv LFlash          // use Internal 10M Flash
// #define Drv LSD           // use SD card

LFile myFile;
char ssid[] = "AndroidAP";  //  your network SSID (name)
char pass[] = "ttkk0914";       // your network password

void setup()
{
    // Open serial communications and wait for port to open:
    Serial.begin(9600);
    Serial.println("setup()");

    // attempt to connect to Wifi network:
    LWiFi.begin();
    while (!LWiFi.connectWPA(ssid, pass))
    {
      delay(1000);
      Serial.println("retry WiFi AP");
    }
    Serial.println("Connected to wifi");
  
    //while(!Serial.available());         // input any thing to start

    Serial.print("Initializing Storage card...");

    pinMode(10, OUTPUT);
    
    Drv.begin();

    // open the file. note that only one file can be open at a time,
    // so you have to close this one before opening another.
    myFile = Drv.open("test.txt", FILE_WRITE);


    // if the file opened okay, write to it:
    if (myFile) {
        Serial.print("Writing to test.txt...");
        printWifiStatus();
        // close the file:
        myFile.close();
        Serial.println("done.");
    } else {
        // if the file didn't open, print an error:
        Serial.println("error opening test.txt");
    }

    // re-open the file for reading:
    myFile = Drv.open("test.txt");
    if (myFile) {
        Serial.println("test.txt:");
        myFile.seek(0);
        // read from the file until there's nothing else in it:
        while (myFile.available()) {            
            Serial.write(myFile.read());
        }
        // close the file:
        myFile.close();
    } else {
        // if the file didn't open, print an error:
        Serial.println("error opening test.txt");
    }
}

void loop()
{
    // nothing happens after setup
}

void printWifiStatus()
{
  // print the SSID of the network you're attached to:
  myFile.print("SSID: ");
  myFile.println(LWiFi.SSID());

  // print your LWiFi shield's IP address:
  IPAddress ip = LWiFi.localIP();
  myFile.print("IP Address: ");
  myFile.println(ip);

  // print the received signal strength:
  long rssi = LWiFi.RSSI();
  myFile.print("signal strength (RSSI):");
  myFile.print(rssi);
  myFile.println(" dBm");
}

